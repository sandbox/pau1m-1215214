<?php

function espeak_settings(){
    $form = array();

    $form['defaults']['espeak_tts_path_to_espeak'] = array(
        '#type' => 'textfield',
        '#title' => 'Path to eSpeak',
        '#default_value' => variable_get('espeak_tts_path_to_espeak', ''),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
        '#description' => t('Enter the full path to the espeak executable e.g. /usr/bin/espeak'),

    );

        $form['defaults']['espeak_tts_path_to_espeak_library'] = array(
        '#type' => 'textfield',
        '#title' => 'Path to eSpeak library directory',
        '#default_value' => variable_get('espeak_tts_path_espeak_library', ''),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
        '#description' => 'The full path to the espeak library directory. <br/>'.
                          'Leave blank to use defaults'

    );
        
       $form['defaults']['espeak_tts_default_espeak_voice'] = array(
        '#type' => 'textfield',
        '#title' => 'Default voice',
        '#default_value' => variable_get('espeak_tts_default_espeak_voice', 'en'),   //pull from user for default
        '#size' => 10,
        '#maxlength' => 20,
        '#required' => FALSE,
        '#description' => 'Use "espeak --voices" on the command line for a complete list of available voices',
    );
       
       
       $form['defaults']['espeak_tts_path_to_lame'] = array(
        '#type' => 'textfield',
        '#title' => 'Path to lame',
        '#default_value' => variable_get('espeak_tts_path_to_lame', ''),   //pull from user for default
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
        '#description' => 'Full path to lame on your system e.g. /usr/bin/lame',
    );  
       
       
       return system_settings_form($form);
}