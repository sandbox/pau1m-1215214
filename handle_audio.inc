<?php

//turn node content plain 
function _espeak_convert_wav_to_mp3($wav = NULL, $mp3 = NULL, $options = array()) {

    if (file_exists($wav)) {
        $lame = variable_get('freetts_path_to_lame', '');
        exec($lame . ' --add-id3v2 ' . $wav . ' ' . $mp3);   //poss add tags here
        return $mp3;
    } else {
        drupal_set_message('TTS Wav file not created. Cannot convert to mp3');
        return false;
    }
}

function _espeak_tts_create_audio_from_node($node = NULL, $nid = 8){
//good enough for the moment
//but not guaranteed to have a body field
//cycle through fields and select which ones to change to audio
//perhaps set an option for this settings for the widget
//so we know wh
  
  dsm(file_get_stream_wrappers());
  
  
  
$node = empty($node) ? node_load($nid) : $node;

//combine contents of title and body fields
$txt = $node->title . '. ' . strip_tags($node->body[$node->language][0]['safe_value']);
//under what circumstances would we want to use a $delta. When would there be multiple values?


//drupal_realpath() // what does it do???


$file_directory_temp = file_directory_temp();
$file_name = _espeak_tts_clean_file_name($node->title);

$full_path_to_txt = file_create_filename($file_name . '.txt', $file_directory_temp);
$full_path_to_wav = file_create_filename($file_name . '.wav', $file_directory_temp); 
$full_path_to_mp3 = file_create_filename($file_name . '.mp3', $file_directory_temp);



file_unmanaged_save_data($txt, 'temporary://' . $file_name . 'xxx.txt', FILE_EXISTS_REPLACE);

//should verify that these exist before going ahead with exec
$espeak  = variable_get('espeak_tts_path_to_espeak', 'espeak'); //@todo add constants for these and move constants to an inc
$voice   = variable_get('espeak_tts_default_espeak_voice', 'en'); //put en value in a constant
$library = variable_get('espeak_tts_path_to_espeak_library', '');
//add options for things like speed and rate 

// if library has a path, build switch for command line
$library = ($library == '') ? '' : ' --path='.$library.' ';

dsm($espeak . $library . ' -v ' . $voice  . ' -f ' . $full_path_to_txt . ' -w ' . $full_path_to_wav);

//run esepak and create a wav file
exec($espeak . $library . ' -v ' . $voice  . ' -f ' . $full_path_to_txt . ' -w ' . $full_path_to_wav);
//how to tell if a command line stuffie has failed or not

     dsm('txt');
     dsm($txt);
     dsm($node);
     
  return 'testing audio creation from node';
  

}

/**
 *
 * @param string $title name to modify
 * @return string 
 * 
 * strips non alpha numeric characters (except - and .) and replace with underscores
 */
function _espeak_tts_clean_file_name($title){

  //replace non-alpha-numeric characters with underscore and make lower case
  //$title =  strtolower(preg_replace("/[^\w\d\.-]/", "_", $title));
  $title =  strtolower(preg_replace("/[^\w\d\.-]/", "_", $title));
  
  //cut length to 200 chars
  return substr($title, 0, 200);

}